#ifndef INTERFACE_H
#define INTERFACE_H

#include <curses.h>
#include <string.h>
#include "../GLOBALS.h"

WINDOW* message_window;
WINDOW* input_window;

void init_ncurses();
void add_msg(char* username, char* text, char* time);

#endif

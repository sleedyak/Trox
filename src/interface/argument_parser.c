#include "argument_parser.h"

void parse_args(int argc, char* argv[])
{
    strcpy(local_username, "Anonymous");
    for(size_t i = 1; i < argc; i++)
    {
	if(strcmp(argv[i], "--username") == 0)
	{
	    strcpy(local_username, argv[++i]);
	}
	if(strcmp(argv[i], "--port") == 0)
	{
	    PORT = (int)(strtol(argv[++i],NULL, 0));
	}
	if(strcmp(argv[i], "--ip") == 0)
	{
	    strcpy(their_ip, argv[++i]);
	}
    }
}

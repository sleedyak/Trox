#include "interface_funcs.h"

void init_ncurses()
{
    initscr();
    cbreak();
    noecho();
    int max_row, max_col;
    getmaxyx(stdscr, max_row, max_col);
    WINDOW* window = newwin(max_row-5, max_col, 2, 0);
    scrollok(window, TRUE);
    WINDOW* window2 = newwin(4, max_col, max_row-4, 0);
    printw("Username: %s", local_username);
    mvprintw(0, max_col/2-2, "Trox");
    mvprintw(0, max_col-(strlen(their_ip)+13), "Connected to %s", their_ip);
    refresh();
    for(int i=0;i<max_col;i++)
    {
	mvprintw(1,i,"-");
    }
    message_window = window;
    input_window = window2;
}

void add_msg(char* username, char* text, char* time)
{
    wprintw(message_window, "[%s] at %s\n%s\n\n", username, time, text);
    wrefresh(message_window);
}

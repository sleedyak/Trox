#ifndef ARGUMENT_PARSER_H
#define ARGUMENT_PARSER_H

#include <stdlib.h>
#include <string.h>
#include "../GLOBALS.h"

void parse_args(int argc, char* argv[]);

#endif

#include "interface/interface_funcs.h"
#include "interface/argument_parser.h"
#include "network/listener/listener.h"
#include "network/sender/sender.h"
#include "network/payload_gen/payload_gen.h"
#include "network/socket_init.h"
#include "GLOBALS.h"
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>

char buffer[PAYLOAD_SIZE+1];

void* listener_starter(void* arg)
{
    start_listener(buffer);
}

void* main_thread_func(void* arg)
{
    send_msg(local_username);
    char input_buffer[PAYLOAD_SIZE-6];
    int wgch;
    int inp_buf_len = 0;
    for(;;)
    {
	wgch = wgetch(input_window);
	if(wgch == 10) //Enter
	{
	    input_buffer[inp_buf_len]='\0';
	    send_msg(payload_gen("03:24",input_buffer));
	    add_msg(local_username, input_buffer, "03:24");
	    memset(input_buffer, 0, PAYLOAD_SIZE-6);
	    inp_buf_len=0;
	}
	else if(wgch == 127) //backspace
	{
	    if(strlen(input_buffer)>0)
	    {
		input_buffer[inp_buf_len-1]='\0';
		--inp_buf_len;
	    }
	}
	else
	{
	    input_buffer[inp_buf_len++] = (char)wgch;
	}
	werase(input_window);
	wprintw(input_window, "%s", input_buffer);
	wrefresh(input_window);
    }
}

void* punch_thread_func(void* args)
{
    for(;;)
    {
	punch_hole();
	sleep(10);
    }
}

void* spam_punch_thread_func(void* args)
{
    spam_punch();
}

void main(int argc, char* argv[])
{
    parse_args(argc, argv);
    if(*their_ip != '\0')
    {
	socket_init();
	pthread_t spam_punch_thread;
	pthread_create(&spam_punch_thread, NULL, spam_punch_thread_func, NULL);
	pthread_join(spam_punch_thread, NULL);
	init_ncurses();
	refresh();
	pthread_t listener_thread, main_thread, punch_thread;
	pthread_create(&listener_thread, NULL, listener_starter, NULL);
	pthread_create(&punch_thread, NULL, punch_thread_func, NULL);
	pthread_create(&main_thread, NULL, main_thread_func, NULL);
	pthread_join(listener_thread, NULL);
    }
    else
    {
	printf("You need to specify IP target, syntax is\ntrox --ip [ip]\n");
    }
}

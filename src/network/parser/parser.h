#ifndef PARSER_H
#define PARSER_H

#include <stdlib.h>
#include <stdio.h> //printf
#include <string.h>
#include "../../GLOBALS.h"

struct parsed_data
{
    char time[sizeof(char)*6];
    char message[PAYLOAD_SIZE-(sizeof(char)*6)];
};

struct parsed_data parse(char* data);

#endif

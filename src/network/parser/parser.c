#include "parser.h"

struct parsed_data parse(char* data)
{
    struct parsed_data parsed;
    strcpy(parsed.time, data);
    strcpy(parsed.message, data+6);
    return parsed;
}

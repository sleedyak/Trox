#include "socket_init.h"

int socket_init()
{
    memset((char*)&destinationAddr,0,sizeof(destinationAddr));
    destinationAddr.sin_family=AF_INET;
    destinationAddr.sin_port=htons(PORT);
    destinationAddr.sin_addr.s_addr=inet_addr(their_ip);

    memset((char*)&localAddr,0,sizeof(localAddr));

    localAddr.sin_family = AF_INET;
    localAddr.sin_port = htons(PORT);
    localAddr.sin_addr.s_addr = htonl(INADDR_ANY); //TODO: I don't think we should listen to any incoming packets but meh, we'll see how it behaves later on 

    if((sockfd=socket(AF_INET, SOCK_DGRAM, 0)) == -1)
    {
	perror("Something went wrong while creating the socket(socket_init): ");
	return 1;
    }

    fcntl(sockfd, F_SETFL, fcntl(sockfd,F_GETFL,0) | O_NONBLOCK); //makes socket non-blocking

    if((bind(sockfd,(struct sockaddr *) &localAddr,sizeof(localAddr))) == -1)
    {
	perror("Binding failed(socket_init): ");
	return 0;
    }
}

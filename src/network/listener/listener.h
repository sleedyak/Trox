#ifndef PACKET_RECEIVER_H
#define PACKET_RECEIVER_H

#define sourceAddr destinationAddr

#include <stdlib.h>
#include <sys/types.h>
#include "../../GLOBALS.h"
#include "../parser/parser.h"
#include "../../interface/interface_funcs.h"
#include "../socket_init.h"


int start_listener(char* buffer);
struct local_info
{
    int port;
    char* buffer;
};

#endif

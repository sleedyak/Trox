#include "listener.h"

int start_listener(char* buffer)
{
    //I felt like destinationAddr was a misleading name, but on the other hand I didn't want to make unnecessary variables so in listener.h
    //there's a line #define sourceAddr destinationAddr to clear the confussion
    int srcLen;

    srcLen = sizeof(sourceAddr);
    struct parsed_data parsed;

    for(;;)
    {
	if((recvfrom(sockfd, buffer, PAYLOAD_SIZE, 0, (struct sockaddr*)&sourceAddr, &srcLen) == -1))
	{
	    perror("recvfrom failed(start_listener): ");
	    return 1;
	}
	wprintw(message_window, "%s", buffer);
	wrefresh(input_window);
	if(*buffer != '\0')
	{
	    if(*their_username == '\0')
	    {
		memcpy(their_username, buffer, strlen(buffer));
		memset(buffer, 0, PAYLOAD_SIZE);
	    }
	    else
	    {
		parsed=parse(buffer);
		add_msg(their_username, parsed.message, parsed.time);
		memset(buffer, 0, PAYLOAD_SIZE);
		memset((char*)&parsed, 0, sizeof(parsed));
	    }
	}
    }
    close(sockfd);
    return 0;
}

#include "payload_gen.h"

char* payload_gen(char* time, char* message)
{
    static char buffer[PAYLOAD_SIZE];
    strcpy(buffer, time);
    buffer[6]='\0';
    strcpy(buffer+6, message);
    return buffer;
}

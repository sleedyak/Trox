#ifndef SOCKET_INIT_H
#define SOCKET_INIT_H

#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include "../GLOBALS.h"

int sockfd;
struct sockaddr_in destinationAddr;
struct sockaddr_in localAddr;
int socket_init();

#endif

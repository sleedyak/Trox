#include "sender.h"

void send_msg(char* string)
{
    if(sendto(sockfd,string,strlen(string)+strlen(string+strlen(string)+1)+1,0,(struct sockaddr*)&destinationAddr,sizeof(destinationAddr)) == -1)
    {
	perror("Something went wrong while sending a packet(send_msg): ");
    }
}

void punch_hole()
{
    if(sendto(sockfd,"\0",1,0,(struct sockaddr*)&destinationAddr,sizeof(destinationAddr)) == -1)
    {
	perror("Something went wrong while sending a packet(punch_hole): ");
    }
}

void spam_punch()
{
    int size = sizeof(destinationAddr);
    char buffer[1];
    for(;;)
    {
	if(recvfrom(sockfd, buffer, 1, 0, (struct sockaddr*)&destinationAddr, &size) == -1)
	{
	    punch_hole();
	    usleep(500);
	}
	else
	{
	    fcntl(sockfd, F_SETFL, fcntl(sockfd,F_GETFL,0) ^ O_NONBLOCK); //makes socket blocking
	    break;
	}
    }
    punch_hole();
}

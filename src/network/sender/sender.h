#ifndef PACKET_SENDER_H
#define PACKET_SENDER_H

#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "../../GLOBALS.h"
#include "../socket_init.h"

void send_msg(char* string);
void punch_hole();
void spam_punch();

struct dest_info
{
    char* string;
    char* ip;
    int port;
} dst_inf;
#endif
